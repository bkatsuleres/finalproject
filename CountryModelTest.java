import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * Test the  class
 */
public class CountryModelTest
{ 
  @Test
  public void testGetCountry1()
  {
    CountryModel country = new CountryModel();
    assertEquals(true, country.getCountry(Cuba);
  }

  @Test
  public void testGetCountry2()
  {
    CountryModel country = new CountryModel();
    assertEquals(false, country.getCountry(ben);
  }

  @Test
  public void testGetCapital()
  {
    CountryModel country = new CountryModel();
    assertEquals("Buenos Aires", country.getCapital(Argentina);
  }

  @Test
  public void testGetTime()
  {
    CountryModel country = new CountryModel();
    assertEquals("UTC+01:00", country.getTime(Germany);
  }

  @Test
  public void testGetCurrency()
  {
    CountryModel country = new CountryModel();
    assertEquals("Brazilian real", country.getCurrency(Brazil);
  }

@Test
  public void testGetLanguage()
  {
    CountryModel country = new CountryModel();
    assertEquals("Spanish", country.getLanguage(Mexico);
  }
}