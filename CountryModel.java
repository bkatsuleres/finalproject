import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javafx.scene.image.Image;

/**
 * Created by Ben Katsuleres on 5/10/2017
 *
 * Model to get country information based on country name.
 */
public class CountryModel {
  private JsonElement jse;

  public boolean getCountry(String country)
  {
    try
    {
      URL wuURL = new URL("https://restcountries.eu/rest/v2/name/" + country + "?fullText=true");

      // Open connection
      InputStream is = wuURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      // Read the results into a JSON Element
      jse = new JsonParser().parse(br);

      // Close connection
      is.close();
      br.close();
    }
    catch (java.io.UnsupportedEncodingException uee)
    {
      uee.printStackTrace();
    }
    catch (java.net.MalformedURLException mue)
    {
      mue.printStackTrace();
    }
    catch (java.io.IOException ioe)
    {
      ioe.printStackTrace();
    }
    catch (java.lang.NullPointerException npe)
    {
      npe.printStackTrace();
    }

    // Check to see if the country was valid.
    return isValid();
  }

  public boolean isValid()
  {
    // If the country is not valid we will get an error field in the JSON
    try {
      String error = jse.getAsJsonObject().get("message").getAsString();
      return false;
    }

    catch (java.lang.NullPointerException npe)
    {
      // We did not see error so this is a valid country
      return true;
    }
  }

  public String getCapital()
  {
    return jse.getAsJsonArray().get(0).getAsJsonObject().get("capital").getAsString();
  }
  
  public String getTime()
  {
    return jse.getAsJsonArray().get(0).getAsJsonObject().get("timezones").getAsJsonArray().get(0).getAsString();
  }

  public String getCurrency()
  {
    return jse.getAsJsonArray().get(0).getAsJsonObject().get("currencies").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString();
  }

  public String getLanguage()
  {
    return jse.getAsJsonArray().get(0).getAsJsonObject().get("languages").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString();
  }
  
  public Image getImage()
  {
    String iconURL = jse.getAsJsonArray().get(0).getAsJsonObject().get("flag").getAsString();
    return new Image(iconURL);
  }
}
