import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


/**
 * Created by Ben Katsuleres on 5/10/2017
 *
 * This is the controller for the FXML document that contains the view. 
 */
public class CountryController implements Initializable {

  @FXML
  private Button btngo;

  @FXML
  private TextField txtcountry;

  @FXML
  private Label lblcapital;
  
  @FXML
  private Label lbltime;

  @FXML
  private Label lblcurrency;

  @FXML
  private Label lbllanguage;

  @FXML
  private ImageView flagcountry;

  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    CountryModel country = new CountryModel();

    // Has the go button been pressed?
    if (e.getSource() == btngo)
    {
      String countryname = txtcountry.getText();
      if (country.getCountry(countryname))
      {
        lblcapital.setText(country.getCapital());
        lbltime.setText(country.getTime());
        lblcurrency.setText(country.getCurrency());
        lbllanguage.setText(country.getLanguage());
        flagcountry.setImage(country.getImage());
      }
      else
      {
        lblcapital.setText("Invalid Country Name");
        lbltime.setText("");
        lblcurrency.setText("");
        lbllanguage.setText("");
        flagcountry.setImage(new Image("Invalid.png"));
      }
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    

}
